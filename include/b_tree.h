# ifndef B_TREE
# define B_TREE

typedef struct  s_node {
    struct s_node   *root;
    struct s_node   *right;
    struct s_node   *left;
    int             value;
}               t_node;

# endif 
